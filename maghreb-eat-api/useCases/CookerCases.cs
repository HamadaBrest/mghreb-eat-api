﻿using System;
using maghreb_eat_api.models;
using Faker;

namespace maghreb_eat_api.useCases
{
    public class CookerCases
    {
        public Cooker[] GetCookers()
        {
            const int cookersNumber = 20;
            Cooker[] cookers = new Cooker[cookersNumber];

            for (int i = 0; i < cookersNumber; i++)
            {
                Cooker cooker = new Cooker(Faker.Name.First(), Faker.Name.Last(), Faker.Address.StreetName(), Faker.Lorem.Paragraph());
                cookers[i] = cooker;
            }

            return cookers;
        }

        public Cooker AddCooker()
        {
            Cooker cooker = new Cooker(Faker.Name.First(), Faker.Name.Last(), Faker.Address.StreetName(), Faker.Lorem.Paragraph());
            return cooker;
        }

        public Cooker GetCooker(int id)
        {
            Cooker cooker = new Cooker(Faker.Name.First(), Faker.Name.Last(), Faker.Address.StreetName(), Faker.Lorem.Paragraph());
            return cooker;
        }
    }
}
