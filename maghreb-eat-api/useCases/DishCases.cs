﻿using System;
using maghreb_eat_api.models;
using Faker;

namespace maghreb_eat_api.useCases
{
    public class DishCases
    {
        public Dish[] GetDishes()
        {
            const int dishesNumber = 20;
            Dish[] dishes = new Dish[dishesNumber];

            for (int i = 0; i < dishesNumber; i++)
            {
                Cooker cooker = new Cooker(Faker.Name.First(), Faker.Name.Last(), Faker.Address.StreetName(), Faker.Lorem.Paragraph());
                Dish dish = new Dish("couscous", "Traditional", "Spicy couscous", Faker.Country.Name(), cooker);
                dishes[i] = dish;
            }

            return dishes;
        }
    }
}
