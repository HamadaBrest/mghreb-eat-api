﻿using System;
namespace maghreb_eat_api.models
{
    public class Dish
    {
        public string id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string description { get; set; }
        public string origin { get; set; }
        public Cooker cooker { get; set; }

        public Dish(string name, string type, string description, string origin, Cooker cooker)
        {
            this.id = Faker.Identification.UsPassportNumber();
            this.name = name;
            this.type = type;
            this.description = description;
            this.origin = origin;
            this.cooker = cooker;
        }
    }
}
