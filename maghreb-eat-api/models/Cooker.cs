﻿using System;
using Faker;

namespace maghreb_eat_api.models
{
    public class Cooker
    {
        public string id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string address { get; set; }
        public string bio { get; set; }

        public Cooker(string firstname, string lastname, string address, string bio)
        {
            this.id = Faker.Identification.UsPassportNumber();
            this.firstname = firstname;
            this.lastname = lastname;
            this.address = address;
            this.bio = bio;
        }
    }
}
