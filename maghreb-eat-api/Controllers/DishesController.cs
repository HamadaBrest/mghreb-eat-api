﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using maghreb_eat_api.models;
using maghreb_eat_api.useCases;

namespace maghreb_eat_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DishesController : ControllerBase
    {
        [HttpGet]
        public Dish[] Get()
        {
            DishCases dishCases = new DishCases();
            Dish[] dishes = dishCases.GetDishes();
            return dishes;
        }
    }
}
