﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using maghreb_eat_api.models;
using maghreb_eat_api.useCases;

namespace maghreb_eat_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CookersController : ControllerBase
    {
        [HttpGet]
        public Cooker[] Get()
        {
            CookerCases cookerCases = new CookerCases();
            Cooker[] cookers = cookerCases.GetCookers();
            return cookers;
        }

        [HttpGet("{id}")]
        public Cooker Get(int id)
        {
            CookerCases cookerCases = new CookerCases();
            Cooker cooker = cookerCases.GetCooker(id);
            return cooker;
        }
    }
}
